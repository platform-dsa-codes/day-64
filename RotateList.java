/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode rotateRight(ListNode head, int k) {
        if (head == null || k == 0) {
            return head;
        }
        
        // Find the length of the linked list
        int length = 1;
        ListNode current = head;
        while (current.next != null) {
            current = current.next;
            length++;
        }
        
        // Calculate the actual rotation amount
        k = k % length;
        if (k == 0) {
            return head;
        }
        
        // Form a circular linked list
        current.next = head;
        
        // Traverse to find the new tail
        int stepsToNewTail = length - k;
        ListNode newTail = head;
        for (int i = 1; i < stepsToNewTail; i++) {
            newTail = newTail.next;
        }
        
        // Update head and break the circular link
        ListNode newHead = newTail.next;
        newTail.next = null;
        
        return newHead;
    }
}
